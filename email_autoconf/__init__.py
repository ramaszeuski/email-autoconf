"""
Copyright (C) 2021 Onlime GmbH <https://www.onlime.ch>

This file is part of email-autoconf.

email-autoconf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

email-autoconf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with email-autoconf. If not, see <https://www.gnu.org/licenses/>.
"""

IDENTIFIER = 'email-autoconf'  # Do not change this!
__version__ = '2021.8'
__author__ = 'Onlime GmbH'

#import logging
# 
# log = logging.getLogger(__name__)
# _handler = logging.StreamHandler()
# _handler.setFormatter(logging.Formatter())
# log.addHandler(_handler)
# log.setLevel(logging.DEBUG)
# log.warning(f'Running {IDENTIFIER} version {__version__}')
