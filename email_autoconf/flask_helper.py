from os import path
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from git import Repo
from flask import Flask

def create_app(settings_override=None):
    """
    Create a Flask application using the app factory pattern.

    Returns:
        object: Flask app
    """
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('email_autoconf.config.settings')
    app.config.from_pyfile('settings.py', silent=True)

    if settings_override: # pragma: no cover
        app.config.update(settings_override)

    app.logger.setLevel(app.config['LOG_LEVEL'])

    if app.config['SENTRY_CONFIG']['enabled']: # pragma: no cover
        sentry_sdk.init(
            dsn=app.config['SENTRY_CONFIG']['dsn'],
            integrations=[FlaskIntegration()],
            release=Repo(search_parent_directories=True).head.object.hexsha,
            environment=app.config['SENTRY_CONFIG']['environment']
        )

    return app
