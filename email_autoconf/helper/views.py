from flask import render_template

class Views(object):
    """
    This object is used to render the views

    Args:
        config (dict): This is the Flask config object

    Attributes:
        config (dict): Variable where config is stored
    """


    def __init__(self, config: object):
        self.config = config

    def render_view(self, view: str, address: str, username: str, display_name: str, uuid: str = None):
        """Renders the given template with the needed variables

        Args:
            view (str): Path to the template which should be rendered
            username (str): Username which should be used
            display_name (str): Display name which should be used
            domain (str): Domain which should be used

        Returns:
            object: The rendered template
        """
        domain = address.split('@')[1]
        return render_template(
            view,
            email_provider = self.config["EMAIL_PROVIDER"],
            address = address,
            domain = domain,
            display_name = display_name,
            username = username,
            imap_server = self.config["IMAP_SERVER_HOSTNAME"] or 'imap.' + domain,
            imap_port = self.config["IMAP_SERVER_PORT"],
            imap_ssl = self.config["IMAP_SERVER_SSL"],
            pop3_server = self.config["POP3_SERVER_HOSTNAME"] or 'pop.' + domain,
            pop3_port = self.config["POP3_SERVER_PORT"],
            pop3_ssl = self.config["POP3_SERVER_SSL"],
            smtp_server = self.config["SMTP_SERVER_HOSTNAME"] or 'smtp.' + domain,
            smtp_port = self.config["SMTP_SERVER_PORT"],
            smtp_ssl = self.config["SMTP_SERVER_SSL"],
            smtp_global_preferred = self.config["SMTP_GLOBAL_PREFERRED"],
            uuid = uuid
        )
