from mysql import connector

class Database (object):
    """
    The Database object represents the connection to the database

    Args:
        config (dict): This is the Flask config object

    Attributes:
        config (dict): Variable where config is stored
    """

    def __init__(self, config: dict, logger: object):
        self.config = config
        self.logger = logger

        if config["DB_TYPE"] == 'mysql': # pragma: no cover
            self.connect_mysql()


    def connect_mysql(self):
        """Connects to the specified MySQL database
        """
        self.logger.debug('Connecting to MySQL DB "{}" on "{}"'.format(self.config["DB_NAME"], self.config["DB_HOST"]))
        self.db = connector.connect(
            host=self.config["DB_HOST"],
            port=self.config["DB_PORT"],
            user=self.config["DB_USER"],
            passwd=self.config["DB_PASSWORD"],
            database=self.config["DB_NAME"]
        )
        self.cursor = self.db.cursor(buffered=True)

    def escape_string(self, string: str):
        """Escapes a string to not harm the database

        Args:
            string (str): String to be escaped

        Returns:
            str: The escaped string
        """
        # return self.db.converter.escape(string)
        return self.db._cmysql.escape_string(string).decode('UTF-8')

    def get_username(self, address: str, default_address: str = None):
        """Gets the user name from the database

        Args:
            address (str): The address for which the username is looked up

        Returns:
            str: The username which matches the address
        """
        if self.config["SELECT_USERNAME"] is None:
            result = default_address or address
            self.logger.debug('Fetching username is disabled. Returning "{}" as username.'.format(result))
            return result

        self.logger.debug('Get username for "{}"'.format(address))
        escaped_address = self.escape_string(address)
        query = self.config["SELECT_USERNAME"].replace('%a', escaped_address)
        self.logger.debug(query)
        self.cursor.execute(query)
        username = self.cursor.fetchone()
        if username is None: # pragma: no cover
            self.logger.info('Did not find the username for address "{}". Returning address "{}"'.format(address, address))
            return address
        else:
            self.logger.debug('Found username "{}" for "{}"'.format(username[0], address))
            return username[0]

    def get_displayname(self, address: str, username: str):
        """Gets the display name from the database

        Args:
            address (str): The address for which the display name is looked up
            username (str): The username for which the display name is looked up

        Returns:
            str: The username which matches the address and/or the username
        """
        if self.config["SELECT_DISPLAY_NAME"] is None:
            self.logger.debug('Fetching display name is disabled. Returning "{}" as display name.'.format(address))
            return address
        
        self.logger.debug('Get display name for address "{}" and/or username "{}"'.format(address, username))
        escaped_address = self.escape_string(address)
        query = self.config["SELECT_DISPLAY_NAME"].replace('%u', username).replace('%a', escaped_address)
        self.logger.debug(query)
        self.cursor.execute(query)

        display_name = self.cursor.fetchone()
        if display_name is None: # pragma: no cover
            self.logger.info('Did not find the display name for address "{}" and/or username "{}". Returning address "{}"'.format(address, username, address))
            return address
        else:
            self.logger.debug('Found display name "{}" for address "{}" and/or username "{}"'.format(display_name[0], address, username))
            return display_name[0]
