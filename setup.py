"""
Copyright (C) 2020 Onlime GmbH <https://www.onlime.ch>

This file is part of email-autoconf.

email-autoconf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

email-autoconf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with email-autoconf. If not, see <https://www.gnu.org/licenses/>.
"""
import setuptools

from email_autoconf import IDENTIFIER, __version__, __author__

with open('README.md', 'r') as f:
    readme = f.read()

setuptools.setup(
    name=IDENTIFIER,
    version=__version__,
    author=__author__,
    author_email='info@onlime.ch',
    description='Mail client autoconfiguration service',
    long_description=readme,
    long_description_content_type='text/markdown',
    url='https://www.onlime.ch/',
    project_urls={
        'Source': 'https://github.com/onlime/email-autoconf',
        'Tracker': 'https://github.com/onlime/email-autoconf/issues',
    },
    packages=setuptools.find_packages(),
    data_files=[
        ('scripts', ['contrib/flask.sh']),
        ('scripts', ['contrib/setupvenv.sh']),
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    install_requires=[
        'Flask>=1.1.1',
        'Jinja2>=2.11.1',
        'xmltodict>=0.12.0',
        'mysql-connector>=2.2.9'
    ],
    python_requires='>=3.7',
)
