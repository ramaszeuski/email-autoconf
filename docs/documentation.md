# email-autoconfig Documentation

[[_TOC_]]

## Installation

> **NOTE:** In below result examples, we use `example.org` as company infrastructure domain where you run **email-autoconf**, and `example.com` as customer domain which is hosted by you.

First, install some required packages, e.g. on Debian Linux:

```bash
# Python3 packages for venv
$ apt install python3-venv python3-pip python3-dev
# uWSGI requirements
$ apt install libssl-dev libpcre3-dev
```

Clone the repo, set up venv, and install required Python modules:

```bash
$ git clone https://gitlab.com/onlime/email-autoconf.git
$ cd email-autoconf
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
```

Copy the example config from `instance/settings.py_example` to `instance/settings.py` and edit it to match your needs:

```python
DB_TYPE = 'mysql'
DB_HOST = '127.0.0.1'
DB_PORT = '3306'
DB_NAME = 'email-autoconf'
DB_USER = 'email-autoconf'
DB_PASSWORD = '************'

SELECT_USERNAME = "SELECT `dest` FROM `mailmap` WHERE `address` = '%a'"
SELECT_DISPLAY_NAME = "SELECT `display_name` FROM `mailaccounts` WHERE `username` = '%u'"

IMAP_SERVER_HOSTNAME = 'imap.example.org'
POP3_SERVER_HOSTNAME = 'pop.example.org'
SMTP_SERVER_HOSTNAME = 'smtp.example.org'

EMAIL_PROVIDER = 'example.org'
```

For all possible options, consult *Configuration* section below.

> Set `DB_TYPE = 'none'` if you don't need any specific email-to-username lookups. This works in an environment where every mailaccount uses its email address as username. 

Now choose one of the 3 available uWSGI instance configurations:

- `uwsgi-http-instance.yml_example`: Testing instance / HTTP port 8080
- `uwsgi-https-instance.yml_example`: HTTPS port 8443
- `uwsgi-socket-instance.yml_example`: Socket port 3032

Use those configurations as examples and deploy the one that fits your need. We recommend to run it as a socket on port 3032 with `uwsgi-socket-instance.yml` instance config and set up an Apache/Nginx reverse proxy (see "Reverse Proxy" section below) in front of it for proper HTTPS support and optimal performance.

For testing, copy `uwsgi-http-instance.yml_example` to `uwsgi-http-instance.yml` and run the instance using [uWSGI](https://uwsgi-docs.readthedocs.io/) as application server:

```bash
$ uwsgi --yaml uwsgi-http.yml --yaml uwsgi-http-instance.yml
```

For basic testing, use `curl` e.g. to retrieve the autoconfig Mozilla scheme:

```bash
curl localhost:8080/mail/config-v1.1.xml?emailaddress=demo@example.com
```

For in-depth testing, use the provided `scripts/email_autoconf_test.py`, see *Testing* section below.

## Configuration

It is possible to configure email-autoconf within the file `instance/settings.py` or with environment variables. See `instance/settings.py_example` for an example configuration.

Environment variables could be handed over if you add them in front of the `uwsgi` command, like this:

```bash
$ SMTP_SERVER=smtp.example.org uwsgi --yaml uwsgi-http.yml --yaml uwsgi-http-instance.yml
```

Settings for the uWSGI instance could be done in a `.yml` file which you then use in the start command. We recommend to consult the documentation on [Configuring uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/Configuration.html) for more details about the uWSGI configuration.

### Parameters

* `DEBUG`: Default `True`; Only for the flask runner. (Could only be configured in  `instance/settings.py`)
* `LOG_LEVEL`: Default `debug`; Possible values: `CRITICAL`, `ERROR`, `WARNING`, `INFO`, `DEBUG`
* `LOG_DIR`: Default `/tmp`; Directory where to store the log file
* `APPLICATION_LOG`: Default `LOG_DIR + '/email-autoconf.log'`; Log file path. (Could only be configured in  `instance/settings.py`)
* `SENTRY_CONFIG`: Defaults to use the following environment variables: (Could only be configured as one in `instance/settings.py`) See config in `config/settings.py`)
* `SENTRY_ENABLED`: Default `False`; To enable use `1` in environment Variable (Only as environment variable)
* `SENTRY_DSN`: Default: `None`; DSN URL for your Sentry project (Only as environment variable)
* `SENTRY_ENV`: Default: `dev`; Sentry environment to push exceptions to (Only as environment variable)
* `DB_TYPE`: Default: `none`; Possible values: `none`, `mysql`
* `DB_HOST`: Default: `127.0.0.1`; Hostname/IP of the database server
* `DB_PORT`: Default: `3306`; Port of the DB server
* `DB_NAME`: Default: `email-autoconf`; Name of the DB to use
* `DB_USER`: Default: `root`; Database user
* `DB_PASS`: Default: empty; Password for the DB user
* `SELECT_USERNAME`: Default: `None`; Select query to get the username. `%a` will be replaced by the requesting email address. No DB query will be executed if `None`. (Example see `instance/settings.py_example`)
* `SELECT_DISPLAY_NAME`: Default: `None`; Select query to get the display name. `%a` will be replaced by the requesting email address. `%u` will be replaced by the username. No DB query will be executed if `None`. (Example see `instance/settings.py_example`)
* `IMAP_SERVER_HOSTNAME`: IMAP server hostname to be used.
* `IMAP_SERVER_PORT`: Default: `993`; IMAP server port to be used.
* `IMAP_SERVER_SSL`: Default: `True`; Use `SSL/TLS` if True otherwise it uses `STARTTLS`.
* `POP3_SERVER_HOSTNAME`: POP3 server hostname to be used.
* `POP3_SERVER_PORT`: Default: `995`; POP3 server port to be used.
* `POP3_SERVER_SSL`: Default: `True`; Use `SSL/TLS` if True otherwise it uses `STARTTLS`.
* `SMTP_SERVER_HOSTNAME`: SMTP server hostname to be used.
* `SMTP_SERVER_PORT`: Default: `465`; SMTP server port to be used.
* `SMTP_SERVER_SSL`: Default: `True`; Use `SSL/TLS` if True otherwise it uses `STARTTLS`.
* `SMTP_GLOBAL_PREFERRED`: Default: `False`; Whether the SMTP server should be used as preferred server on the client or not.
* `EMAIL_PROVIDER`: Default: `example.com`; Name/Domain of the email provider.

## Systemd Service

We recommend to run email-autoconf as a Systemd service for production. The following setup has been proved working on a Debian Linux (tested on Debian 10 Buster).

> **IMPORTANT: Don’t run as `root`!**
>
> If you use a port number greater than 1024 (I suggest port 3032), the application does not require super user privileges when running. It also does not need to be installed as root. It is recommended that you create a user account specifically for  email-autoconf, but other unprivileged users will do as well.

Create `/etc/systemd/system/email-autoconf.service`:

```ini
[Unit]
Description=uWSGI instance to serve email-autoconf
After=network.target

[Service]
Type=simple
User=email-autoconf
Group=email-autoconf
WorkingDirectory=/var/www/email-autoconf
ExecStart=/var/www/email-autoconf/venv/bin/uwsgi --yaml uwsgi-http.yml --yaml uwsgi-socket-instance.yml
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

After creating this new Systemd service unit, run:

```bash
$ systemctl daemon-reload
```

Then, enable/start the service:

```bash
$ systemctl enable email-autoconf.service
$ systemctl start email-autoconf.service
$ systemctl status email-autoconf.service
```

email-autoconf should now in socket mode on port 3032 (as we have provided`uwsgi-socket-instance.yml` instance config in Systemd `ExecStart` command).

## DNS Configuration

On your companies infrastructure DNS zone (the domain you usually put into `EMAIL_PROVIDER` in your instance config), you should configure the following DNS resource records:

```bind
email-autoconf	IN	A	X.X.X.X. ; IP of the server running email-autoconf

maildiscovery	IN	CNAME	email-autoconf.example.org.
autoconfig	IN	CNAME	maildiscovery.example.org.
autodiscover	IN	CNAME	maildiscovery.example.org.
```

On any customer domain, the following records should be configured:

```
autoconfig	300	IN	CNAME	maildiscovery.example.org.
_autodiscover._tcp	300	IN	SRV	0 0 443 maildiscovery.example.org.
```

## Reverse Proxy

To simplify SSL handling and protect the uWSGI server, we strongly recommend to use some sort of reverse proxy in front of email-autoconf. Below you can find config examples for Apache and Nginx.

> NOTE: Below examples are based on the recommended **`uwsgi-socket-instance.yml` uWSGI instance configuration** for email-autoconf, running it as a **socket on port 3032**.

### Apache

In Apache, the modules [`proxy`](https://httpd.apache.org/docs/2.4/mod/mod_proxy.html) and [`proxy_uwsgi`](https://httpd.apache.org/docs/2.4/mod/mod_proxy_uwsgi.html) has to be enabled, while `proxy` should already be enabled by default.

```bash
$ a2enmod proxy_uwsgi
$ systemctl restart apache2
```

Apache vitual hosts to proxy requests to the email-autoconf uWSGI server could look like this:

```apache
<VirtualHost *:80>
    ServerName autoconfig.example.org
    ServerAlias autoconfig.*
    ServerAlias maildiscovery.example.org maildiscovery.*
    ServerAlias autodiscover.example.org autodiscover.*
    ErrorLog ${APACHE_LOG_DIR}/email_autoconf_error.log
    CustomLog ${APACHE_LOG_DIR}/email_autoconf_access.log combined
    ServerAdmin hostmaster@example.org
    
    ProxyPass / uwsgi://127.0.0.1:3032/
</VirtualHost>

<VirtualHost *:443>
    ServerName autoconfig.example.org
    ServerAlias autoconfig.*
    ServerAlias maildiscovery.example.org maildiscovery.*
    ServerAlias autodiscover.example.org autodiscover.*
    SSLEngine on
    SSLCertificateKeyFile /etc/letsencrypt/live/maildiscovery.example.org/privkey.pem
    SSLCertificateFile /etc/letsencrypt/live/maildiscovery.example.org/fullchain.pem
    ErrorLog ${APACHE_LOG_DIR}/email_autoconf_error.log
    CustomLog ${APACHE_LOG_DIR}/email_autoconf_access.log combined
    ServerAdmin hostmaster@example.com
    
    ProxyPass / uwsgi://127.0.0.1:3032/
</VirtualHost>
```


### Nginx

For Nginx a config like this should work:

```nginx
server {
    listen 80;
    server_name autoconfig.example.org autoconfig.* maildiscovery.* autodiscovery.* autodiscover.*;
    location / {
        proxy_pass http://127.0.0.1:3032/;
    }
}

server {
    listen 443 ssl;
    server_name autoconfig.example.org autoconfig.* maildiscovery.* autodiscovery.* autodiscover.*;
    ssl_certificate_key /etc/letsencrypt/live/maildiscovery.example.org/privkey.pem;
    ssl_certificate /etc/letsencrypt/live/maildiscovery.example.org/fullchain.pem;
    ssl_protocols TLSv1.2 TLSv1.3;
    location / {
        proxy_pass http://127.0.0.1:3032/;
    }
}
```

Check if email-autoconf is running (presents a nice splash screen with Onlime logo): https://maildiscovery.example.org/

## Testing

### email_autoconf_test.py

Use `scripts/email_autoconf_test.py` helper script to test any self-hosted or external email address. Make sure you execute that script under your `venv` which is correctly set up as described in above installation section:

```bash
$ cd /var/www/email-autoconf
$ . venv/bin/activate
(venv)$ cd scripts
(venv)$ python3 email_autoconf_test.py demo@example.com
```

Sample result (full XML output for all 3 protocols Mozilla Autoconfig, Microsoft Autodiscover, Apple Mobileconfig):

```xml
Testing Autoconfig
Try https://autoconfig.example.org/mail/config-v1.1.xml?emailaddress=demo@example.com
Autoconfig content:

<?xml version="1.0" encoding="UTF-8"?>
<clientConfig version="1.1">
    <emailProvider id="example.org">
      <domain>example.com</domain>
      <displayName>demo@example.com</displayName>
      <incomingServer type="imap">
         <hostname>imap.example.org</hostname>
         <port>993</port>
         <socketType>SSL</socketType>
         <authentication>password-cleartext</authentication>
         <username>demo_example_com</username>
      </incomingServer>
      <incomingServer type="pop3">
         <hostname>pop.example.org</hostname>
         <port>995</port>
         <socketType>SSL</socketType>
         <authentication>password-cleartext</authentication>
         <username>demo_example_com</username>
      </incomingServer>
      <outgoingServer type="smtp">
         <hostname>smtp.example.org</hostname>
         <port>465</port>
         <socketType>SSL</socketType>
         <username>demo_example_com</username>
         <authentication>password-cleartext</authentication>
         <useGlobalPreferredServer>no</useGlobalPreferredServer>
      </outgoingServer>
    </emailProvider>
</clientConfig>

---
Testing Autodiscover
Try https://maildiscovery.example.org/autodiscover/autodiscover.xml
Autodiscover content:

<?xml version="1.0" encoding="UTF-8"?>
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
  <Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
    <User>
      <DisplayName>demo@example.com</DisplayName>
    </User>
    <Account>
      <AccountType>email</AccountType>
      <Action>settings</Action>
      <Protocol>
        <Type>IMAP</Type>
        <Server>imap.example.org</Server>
        <Port>993</Port>
        <SPA>off</SPA>
        <Encryption>Auto</Encryption>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
        <DomainRequired>off</DomainRequired>
        <LoginName>demo_example_com</LoginName>
      </Protocol>
      <Protocol>
        <Type>POP3</Type>
        <Server>pop.example.org</Server>
        <Port>995</Port>
        <SPA>off</SPA>
        <Encryption>Auto</Encryption>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
        <DomainRequired>off</DomainRequired>
        <LoginName>demo_example_com</LoginName>
      </Protocol>
      <Protocol>
        <Type>SMTP</Type>
        <Server>smtp.example.org</Server>
        <Port>587</Port>
        <SPA>off</SPA>
        <Encryption>Auto</Encryption>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
        <UsePOPAuth>on</UsePOPAuth>
        <SMTPLast>off</SMTPLast>
        <DomainRequired>off</DomainRequired>
        <LoginName>demo_example_com</LoginName>
      </Protocol>
    </Account>
  </Response>
</Autodiscover>

---
Testing Mobileconfig
Try https://autoconfig.example.org/email.mobileconfig?email=demo@example.com
(...)
```

## Docker

It is also possible to run email-autoconf as Docker container.

> Be aware that this scenario is not fully tested!

To run this in a Docker container you need the following steps:

1. Build the Docker image: `docker-compose build`
2. Run the image with the needed environment variables:
```bash
$ docker run -d -e IMAP_SERVER_HOSTNAME=imap.example.com -e POP3_SERVER_HOSTNAME=pop.example.com -e SMTP_SERVER_HOSTNAME=smtp.example.com -p 8080:8080 --name email-autoconf email-autoconf
```
3. Test it with curl: `curl localhost:8080/mail/config-v1.1.xml?emailaddress=demo@example.com`

You can also configure a docker instance by binding the instance folder to it. For this, please add your config to `instance/settings.py` like described in the [README](../README.md). Now you can run the container with this command:

```bash
$ docker run -d -v $(pwd)/instance:/app/instance -p 8080:8080 --name email-autoconf email-autoconf
```

