#!/bin/bash

coverage run tests.py
coverage xml -i -o /tmp/coverage.xml

sed -i -e 's/\/app/\/root\/src/' /tmp/coverage.xml
