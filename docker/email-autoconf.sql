--
-- Table structure for table `mailmap`
--

DROP TABLE IF EXISTS `mailmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailmap` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL DEFAULT '',
  `dest` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Test data for table `mailmap`
--

LOCK TABLES `mailmap` WRITE;
INSERT INTO `mailmap` (`address`, `dest`) VALUES 
  ('testaddress@example.com', 'testuser'),
  ('testaddress2@example.com', 'testuser2'),
  ('testaddress3@example.com', 'testaddress3@example.com');
UNLOCK TABLES;
