#!/bin/bash

# Set permissions for static (to avoid Docker permission problems when generating screen.css)
chown -R email-autoconf:email-autoconf /app/email_autoconf/static
