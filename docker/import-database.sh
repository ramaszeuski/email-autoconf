#!/bin/bash

while ! nc -z db 3306;
do
    echo "waiting for MySQL startup ...";
    sleep 1;
done;

cat /app/docker/email-autoconf.sql | mysql -u root -h db email-autoconf
