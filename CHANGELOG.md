# CHANGELOG

## [2021.x (Unreleased)](https://gitlab.com/onlime/email-autoconf/compare/2021.8...main)

## [2021.8 (2021-08-25)](https://gitlab.com/onlime/email-autoconf/-/tags/2021.8)

- First public release

